package com.infocertselezione.cardealer.service;

import com.infocertselezione.cardealer.repository.CarRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@TestPropertySource(locations="classpath:application.properties")
@TestPropertySource(properties="app.uload.dir=C:\\test-dir")
@ExtendWith(MockitoExtension.class)
public class CarServiceImplTest {

    @Mock
    private CarRepository carRepository;

    @InjectMocks
    private CarServiceImpl carService;

    private MockMultipartFile file;

    @SneakyThrows
    @BeforeEach
    public void setUp() {
        String content = "modello_auto,is4x4,fasciaDiPrezzo,anzianita,automaticTransmission,fuelType\n" +
                "Model1,Yes,High,New,Yes,Diesel\n" +
                "Model2,No,Low,Old,No,Petrol";
        InputStream inputStream = new ByteArrayInputStream(content.getBytes());
        file = new MockMultipartFile("file", "temp.csv", "text/csv", inputStream);
    }


    @Test
    public void testBulkUploadWhenInvalidFileThenError() throws IOException {
        file = new MockMultipartFile("file", "temp.csv", "text/csv", new ByteArrayInputStream(new byte[0]));

        ResponseEntity<?> response = carService.bulkUpload(file);

        verify(carRepository, never()).saveAll(anyList());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Errore durante la lettura del file CSV.", response.getBody());
    }

    @Test
    public void testBulkUploadWhenEmptyFileThenError() throws IOException {
        file = new MockMultipartFile("file", "temp.csv", "text/csv", new ByteArrayInputStream(new byte[0]));

        ResponseEntity<?> response = carService.bulkUpload(file);

        verify(carRepository, never()).saveAll(anyList());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Errore durante la lettura del file CSV.", response.getBody());
    }

}
