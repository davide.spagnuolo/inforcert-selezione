package com.infocertselezione.cardealer.controller;

import com.infocertselezione.cardealer.model.Car;
import com.infocertselezione.cardealer.repository.CarRepository;
import com.infocertselezione.cardealer.service.CarService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
@Tag(name = "Car API", description = "Operazioni CRUD per le auto")
@RestController
@RequestMapping("/api/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    CarRepository carRepository;

    @RequestMapping(path = "/bulk-upload",method = RequestMethod.POST ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Caricamento completato con successo"),
            @ApiResponse(responseCode = "500", description = "Errore durante il caricamento")
    })
    public ResponseEntity<String> bulkUpload(@Parameter(description = "csv file",required = true, content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE)) @RequestParam("file") MultipartFile file) {
        carService.bulkUpload(file);
        return ResponseEntity.ok("Bulk upload completed");
    }

    @GetMapping("/filtroCustom")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Risultato della ricerca"),
            @ApiResponse(responseCode = "500", description = "Errore durante il filtraggio")
    })
    public List<Car> filtroCustom(
            @Parameter(description = "Search Car by paramiter Anzianità")  @RequestParam(value = "anzianita",required = false) String anzianita,
            @Parameter(description = "Search Car by paramiter fuelType")    @RequestParam(value = "fuelType",required = false) String fuelType,
            @Parameter(description = "Search Car by paramiter automaticTransmission")   @RequestParam(value = "automaticTransmission",required = false) String automaticTransmission,
            @Parameter(description = "Search Car by paramiter is4x4 ") @RequestParam(value = "is4x4",required = false) String is4x4,
            @Parameter(description = "Search Car by paramiter fasciaPrezzo")   @RequestParam(value = "fasciaPrezzo",required = false) String fasciaPrezzo
    ) {
        return carRepository.findCustomFilteredCars(anzianita, fuelType, automaticTransmission, is4x4,fasciaPrezzo);
    }
}
