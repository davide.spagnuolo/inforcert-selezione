package com.infocertselezione.cardealer.repository;

import com.infocertselezione.cardealer.model.Car;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepository extends MongoRepository<Car, String>,CarRepositoryCustom  {
}
