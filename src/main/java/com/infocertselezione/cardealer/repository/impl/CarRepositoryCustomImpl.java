package com.infocertselezione.cardealer.repository.impl;

import com.infocertselezione.cardealer.model.Car;
import com.infocertselezione.cardealer.repository.CarRepositoryCustom;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryCustomImpl implements CarRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public CarRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Car> findCustomFilteredCars(String anzianita, String fuelType, String automaticTransmission, String is4x4,String fasciaPrezzo) {
        Criteria criteria = new Criteria();

        if (StringUtils.isNotBlank(anzianita)) {
            criteria.and("anzianita").is(anzianita);
        }

        if (StringUtils.isNotBlank(fuelType)) {
            criteria.and("fuelType").is(fuelType);
        }

        if (automaticTransmission != null) {
            criteria.and("automaticTransmission").is(automaticTransmission);
        }

        if (is4x4 != null) {
            criteria.and("is4x4").is(is4x4);
        }

        if (StringUtils.isNotBlank(fasciaPrezzo)) {
            criteria.and("fasciaDiPrezzo").is(fasciaPrezzo);
        }

        Query query = new Query(criteria);
        return mongoTemplate.find(query, Car.class);
    }
}

