package com.infocertselezione.cardealer.repository;

import com.infocertselezione.cardealer.model.Car;

import java.util.List;

public interface CarRepositoryCustom {
    List<Car> findCustomFilteredCars(String anzianita, String fuelType, String automaticTransmission, String is4x4, String fasciaPrezzo);
}
