package com.infocertselezione.cardealer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    private String modelloAuto;
    private String is4x4;
    private String fasciaDiPrezzo;
    private String anzianita;
    private String automaticTransmission;
    private String fuelType;
}
