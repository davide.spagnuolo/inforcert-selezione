package com.infocertselezione.cardealer.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface CarService {

    ResponseEntity<?> bulkUpload(MultipartFile file);

}
