package com.infocertselezione.cardealer.service;

import com.infocertselezione.cardealer.model.Car;
import com.infocertselezione.cardealer.repository.CarRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Value("${app.upload.dir}")
    private String uploadDir;

    @Autowired
    CarRepository carRepository;

    @Override
    public ResponseEntity<?> bulkUpload(MultipartFile file) {
        try {
            File tempFile = new File(uploadDir + File.separator + "temp-csv-" + System.currentTimeMillis() + ".csv");
            Files.copy(file.getInputStream(), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            List<Car> cars = mapCsvToCars(tempFile);
            carRepository.saveAll(cars);
            tempFile.delete();
            return new ResponseEntity<>("File CSV letto con successo.", HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Errore durante la lettura del file CSV.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private List<Car> mapCsvToCars(File file) throws IOException {
        try (CSVReader csvReader = new CSVReaderBuilder(Files.newBufferedReader(file.toPath())).build()) {
            List<String[]> records = csvReader.readAll();
            // Salta l'intestazione del CSV se presente
            if (!records.isEmpty() && records.get(0)[0].equalsIgnoreCase("modello_auto")) {
                records.remove(0);
            }
            // Mappa i record CSV agli oggetti Car
            return records.stream()
                    .map(this::mapRecordToCar)
                    .toList();
        } catch (CsvException e) {
            throw new RuntimeException(e);
        }
    }

    private Car mapRecordToCar(String[] record) {
        return Car.builder()
                .modelloAuto(record[0])
                .is4x4(record[1])
                .fasciaDiPrezzo(record[2])
                .anzianita(record[3])
                .automaticTransmission(record[4])
                .fuelType(record[5])
                .build();
    }
}
