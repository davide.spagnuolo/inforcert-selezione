
1) Assicurarsi di avere docker installato sul proprio PC altrimenti https://docs.docker.com/engine/install/
2) Scaricare il progetto dal mio repository pubblico https://gitlab.com/davide.spagnuolo/inforcert-selezione
3) Aprire il progetto con un ide (IntelliJ o nel caso siate poveri va bene anche Eclipse)
4) Recatevi sotto il folder di progetto resources MongoDB aprite la path con power shell o con il command prompt
![img.png](img.png)
5) date il comando docker-compose up -d per creare un immagine di mongo db.
6) Verificate che il container sia stato creato e startato con il comando docker ps
7) ![img_1.png](img_1.png)
8) scaricate mongo db compass (che non serve a fare un finaziamento) dal link https://www.mongodb.com/try/download/compass installatelo.
9) Apritelo e connettetevi all'istanza di mongo inserendo questa connection string mongodb://infocert:infocert@localhost:27017
10) ![img_2.png](img_2.png) 
11) una volta connessi create il database che ospiter� i nostri dati chiamandolo -->  car-dealer vuole anche il nome di una collection mettete test anche se a noi non serve a nulla.
12) ![img_3.png](img_3.png)
13) una volta creato il db siamo pronti a eseguire il servizio.
14) il servizo � stato sviluppato con JDK 17 e possibile runnarlo sia dal ide oppure dopo aver effettuato la build mvn clean install si puo avviare con il comando java -jar car-dealer-0.0.1-SNAPSHOT.jar 
15) una volta avviato potete aprire lo swagger al seguente link : http://localhost:8090/swagger-ui/index.html#/
16) il csv da caricare lo trovate nel progetto nel folder resources/csv/Cars.csv
17) prima di caricare il file createvi una directory dove verra srcitto il file temporaneo e aggiornate la path nell' application.yml
18) ![img_4.png](img_4.png)
19) cosiglio di farlo sotto c: sul desktop potrebbe avere dei problemi.
20) dopo aver caricato il file potete testare i filtri o dallo swagger oppure ho agg una collection postman sempre sotto resource/postman/Infocert-Test-Car-Dealer.postman_collection.json
21) FINE
